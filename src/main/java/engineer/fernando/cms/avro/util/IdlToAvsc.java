package engineer.fernando.cms.avro.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.avro.tool.IdlToSchemataTool;

/**
 * Converts an entire directory from Avro IDL (.avdl) to schema (.avsc)
 * http://www.virtualroadside.com/blog/index.php/2014/06/08/automatically-generating-avro-schemata-avsc-files-using-maven/
 */
public class IdlToAvsc {

    public static void main(String [] args) throws Exception {
        IdlToSchemataTool tool = new IdlToSchemataTool();

        File inDir = new File(args[0]);
        File outDir = new File(args[1]);

        for (File inFile: inDir.listFiles()) {
            List<String> toolArgs = new ArrayList<String>();
            toolArgs.add(inFile.getAbsolutePath());
            toolArgs.add(outDir.getAbsolutePath());

            tool.run(System.in, System.out, System.err, toolArgs);
        }
    }

}
